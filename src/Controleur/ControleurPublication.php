<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurServiceInterface;

class ControleurPublication extends ControleurGenerique
{

    public function __construct(private PublicationServiceInterface $publicationServiceInterface) {}

    #[Route(path: '/', name:'accueil', methods:["GET"])]
    #[Route(path: '/publications', name:'afficherListe', methods:["GET"])]
    public function afficherListe(): Response
    {
        $publications = $this->publicationServiceInterface->recupererPublications();
        return $this->afficherTwig('publication/feed.html.twig', [
            "publications" => $publications
        ]);

    }

    #[Route(path: '/publications', name:'afficherFormulaireCreation3', methods:["POST"])]
    public function creerDepuisFormulaire(): RedirectResponse
    {
        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $message = $_POST['message'];

        try{
            $this->publicationServiceInterface->creerPublication($idUtilisateurConnecte, $message);
            return $this->rediriger('afficherListe');
        } catch (ServiceException $e) {
            // Si une exception est attrapée, ajouter le message en tant que message flash
            MessageFlash::ajouter("error", $e->getMessage());
        }

        return $this->rediriger('afficherListe');
    }


}