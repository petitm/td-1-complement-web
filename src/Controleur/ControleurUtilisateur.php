<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurService;
use TheFeed\Service\UtilisateurServiceInterface;

class ControleurUtilisateur extends ControleurGenerique
{
    public function __construct(private PublicationServiceInterface $publicationServiceInterface,
                                private UtilisateurServiceInterface $utilisateurServiceInterface) {}

    public function afficherErreur($messageErreur = "", $controleur = ""): Response
    {
        return parent::afficherErreur($messageErreur, "utilisateur");
    }

    #[Route(path: '/utilisateurs/{idUtilisateur}/publications', name:'afficherPublications', methods:["GET"])]
    public function afficherPublications($idUtilisateur): Response
    {
        try {
            $utilisateur = $this->utilisateurServiceInterface->recupererUtilisateurParId($idUtilisateur, false);
            $loginHTML = htmlspecialchars($utilisateur->getLogin());
            $publications = $this->publicationServiceInterface->recupererPublicationsUtilisateur($idUtilisateur);
            return $this->afficherTwig('publication/page_perso.html.twig', [
                "publications" => $publications,
                "connected_user_id" => $loginHTML]);
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", "Login inconnu.");
            return $this->rediriger("afficherListe");
        }
    }

    #[Route(path: '/inscription', name:'afficherFormulaireCreation', methods:["GET"])]
    public function afficherFormulaireCreation(): Response
    {
        return $this->afficherTwig('utilisateur/inscription.html.twig');
    }

    #[Route(path: '/inscription', name:'afficherFormulaireCreation2', methods:["POST"])]
    public function creerDepuisFormulaire(): RedirectResponse
    {
        $login = $_POST['login'] ?? null;
        $motDePasse = $_POST['mot-de-passe'] ?? null;
        $adresseMail = $_POST['email'] ?? null;
        $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'] ?? null;
        try {
            $this->utilisateurServiceInterface->creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil);
            MessageFlash::ajouter("success", "L'utilisateur a bien été créé !");
            return $this->rediriger("afficherListe");
        }
        catch (ServiceException $e){
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger("afficherFormulaireCreation2");
        }
    }
    #[Route(path: '/connexion', name:'afficherFormulaireConnexion', methods:["GET"])]
    public function afficherFormulaireConnexion(): Response
    {
        return $this->afficherTwig('utilisateur/connexion.html.twig');
    }

    #[Route(path: '/connexion', name:'connecter', methods:["POST"])]
    public function connecter(): RedirectResponse
    {
        if (!(isset($_POST['login']) && isset($_POST['mot-de-passe']))) {
            MessageFlash::ajouter("error", "Login ou mot de passe manquant.");
            return $this->rediriger("afficherFormulaireConnexion");
        }
        $login = $_POST['login'];
        $motDePasse = $_POST['mot-de-passe'];

        try {
            $this->utilisateurServiceInterface->connecterUtilisateur($login, $motDePasse);
            MessageFlash::ajouter("success", "Connexion effectuée.");
            return $this->rediriger("afficherListe");
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger("afficherFormulaireConnexion");
        }
    }

    #[Route(path: '/deconnexion', name:'deconnecter', methods:["GET"])]
    public function deconnecter(): RedirectResponse
    {
        try{
            $this->utilisateurServiceInterface->deconnecterUtilisateur();
            MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
        }catch (ServiceException $e){
            MessageFlash::ajouter("error", $e->getMessage());
        }
        return $this->rediriger("afficherListe");
    }
}
