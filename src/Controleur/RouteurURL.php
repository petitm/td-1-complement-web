<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\Conteneur;
use TheFeed\Lib\MessageFlash;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\AttributeDirectoryLoader;
use TheFeed\Lib\AttributeRouteControllerLoader;
use TheFeed\Controleur\ControleurPublication;
use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\PublicationService;
use TheFeed\Service\UtilisateurService;
use TheFeed\Configuration\ConfigurationBDDMySQL;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class RouteurURL
{
    private static MessageFlash $messageFlash;
    public static function traiterRequete() {
        $conteneur = new ContainerBuilder();

        $conteneur->register('configuration_bdd_my_sql', ConfigurationBDDMySQL::class);

        $connexionBaseService = $conteneur->register('connexion_base_de_donnees', ConnexionBaseDeDonnees::class);
        $connexionBaseService->setArguments([new Reference('configuration_bdd_my_sql')]);

        $publicationsRepositoryService = $conteneur->register('publication_repository',PublicationRepository::class);
        $publicationsRepositoryService->setArguments([new Reference('connexion_base_de_donnees')]);

        $utilisateurRepositoryService = $conteneur->register('utilisateur_repository',UtilisateurRepository::class);
        $utilisateurRepositoryService->setArguments([new Reference('connexion_base_de_donnees')]);

        $publicationService = $conteneur->register('publication_service', PublicationService::class);
        $publicationService->setArguments([new Reference('publication_repository'), new Reference('utilisateur_repository')]);

        $publicationService = $conteneur->register('utilisateur_service', UtilisateurService::class);
        $publicationService->setArguments([new Reference('utilisateur_repository')]);

        $publicationControleurService = $conteneur->register('controleur_publication',ControleurPublication::class);
        $publicationControleurService->setArguments([new Reference('publication_service')]);

        $publicationControleurService = $conteneur->register('controleur_utilisateur',ControleurUtilisateur::class);
        $publicationControleurService->setArguments([new Reference('publication_service'), new Reference('utilisateur_service')]);


        $requete = Request::createFromGlobals();

        $connectedUserId = (new ConnexionUtilisateur())->getIdUtilisateurConnecte();

        $fileLocator = new FileLocator(__DIR__);
        $attrClassLoader = new AttributeRouteControllerLoader();
        $routes = (new AttributeDirectoryLoader($fileLocator, $attrClassLoader))->load(__DIR__);

        $contexteRequete = (new RequestContext())->fromRequest($requete);

        $generateurUrl = new UrlGenerator($routes, $contexteRequete);
        $assistantUrl = new UrlHelper(new RequestStack(), $contexteRequete);

        $twigLoader = new FilesystemLoader(__DIR__ . '/../vue/');
        $twig = new Environment(
            $twigLoader,
            [
                'autoescape' => 'html',
                'strict_variables' => true
            ]
        );
        $twig->addFunction(new TwigFunction('route', $generateurUrl->generate(...)));
        $twig->addFunction(new TwigFunction('asset', $assistantUrl->getAbsoluteUrl(...)));
        $twig->addGlobal('connected_user_id', $connectedUserId);
        self::$messageFlash = new MessageFlash();
        $twig->addGlobal('messagesFlash', self::$messageFlash);

        Conteneur::ajouterService("twig", $twig);

        Conteneur::ajouterService("generateurUrl", $generateurUrl);
        Conteneur::ajouterService("assistantUrl", $assistantUrl);

        try {
            $associateurUrl = new UrlMatcher($routes, $contexteRequete);
            $donneesRoute = $associateurUrl->match($requete->getPathInfo());
            // NoConfigurationException If no routing configuration could be found
            // ResourceNotFoundException If the resource could not be found
            // MethodNotAllowedException If the resource was found but the request method is not allowed
            // LogicException If a controller was found based on the request, but it is not callable
            // RuntimeException When no value could be provided for a required argument
            $requete->attributes->add($donneesRoute);

            $resolveurDeControleur = new ContainerControllerResolver($conteneur);
            $controleur = $resolveurDeControleur->getController($requete);

            $resolveurDArguments = new ArgumentResolver();
            $arguments = $resolveurDArguments->getArguments($requete, $controleur);

            $reponse = call_user_func_array($controleur, $arguments);
        } catch (ResourceNotFoundException $exception) {
            // Remplacez xxx par le bon code d'erreur
            $reponse = (new ControleurGenerique())->afficherErreur($exception->getMessage(), 404);
        } catch (MethodNotAllowedException $exception) {
            // Remplacez xxx par le bon code d'erreur
            $reponse = (new ControleurGenerique())->afficherErreur($exception->getMessage(), 405);
        } catch (\Exception $exception) {
            $reponse = (new ControleurGenerique())->afficherErreur($exception->getMessage()) ;
        }
        $reponse->send();
    }
}