<?php
namespace TheFeed\Service;
use TheFeed\Controleur\ControleurGenerique;
use TheFeed\Controleur\ControleurPublication;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;

class PublicationService implements PublicationServiceInterface
{
    public function __construct(private PublicationRepositoryInterface $publicationRepositoryInterface,
                                private UtilisateurRepositoryInterface $utilisateurRepositoryInterface) {}
    public function recupererPublicationsUtilisateur($idUtilisateur){
        return $this->publicationRepositoryInterface->recupererParAuteur($idUtilisateur);
    }
    public function recupererPublications(): array{
        return $this->publicationRepositoryInterface->recuperer();
    }
    public function creerPublication($idUtilisateur, $message) : void {
        $utilisateur = $this->utilisateurRepositoryInterface->recupererParClePrimaire($idUtilisateur);

        if ($utilisateur == null) {
            throw new ServiceException("non connecté");
        }
        if ($message == null || $message == "") {
            throw new ServiceException("Message vide");
        }
        if (strlen($message) > 250) {
            throw new ServiceException("Message trop long");
        }

        $publication = Publication::create($message, $utilisateur);
        $this->publicationRepositoryInterface->ajouter($publication);
    }
}