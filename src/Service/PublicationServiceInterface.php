<?php

namespace TheFeed\Service;

interface PublicationServiceInterface
{
    public function recupererPublicationsUtilisateur($idUtilisateur);

    public function recupererPublications(): array;

    public function creerPublication($idUtilisateur, $message): void;
}