<?php
namespace TheFeed\Service;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService implements UtilisateurServiceInterface
{
    public function __construct(private UtilisateurRepositoryInterface $utilisateurRepositoryInterface) {}
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil) : void
    {
        if (
            isset($login) && isset($motDePasse) && isset($email)
            && isset($donneesPhotoDeProfil)
        ) {
            //Verifier la taille du login
            if (strlen($login) < 4 || strlen($login) > 20) {
                throw new ServiceException("Le login doit être compris entre 4 et 20 caractères!");            }
            //Verifier la validité du mot de passe
            if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
                throw new ServiceException("Le mot de passe doit contenir entre 8 et 20 caractères, au moins une minuscule, une majuscule et un nombre");
            }
            //Verifier le format de l'adresse mail
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new ServiceException("L'adresse mail est incorrecte");
            }
            //Verifier que l'utilisateur n'existe pas déjà
            $utilisateur = $this->utilisateurRepositoryInterface->recupererParLogin($login);
            if ($utilisateur != null) {
                throw new ServiceException("Login déjà pris");
            }
            //Verifier que l'adresse mail n'est pas prise
            $utilisateurMail = $this->utilisateurRepositoryInterface->recupererParEmail($email);
            if ($utilisateurMail != null) {
                throw new ServiceException("email déjà pris");
            }
            //Verifier extension photo de profil
            $explosion = explode('.', $donneesPhotoDeProfil['name']);
            $fileExtension = end($explosion);
            if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
                throw new ServiceException("Photo au mauvais format");
            }
            //Enregistrer la photo de profil
            $pictureName = uniqid() . '.' . $fileExtension;
            $from = $donneesPhotoDeProfil['tmp_name'];
            $to = __DIR__ . "/../../ressources/img/utilisateurs/$pictureName";
            move_uploaded_file($from, $to);
            //Chiffrer le mot de passe
            $mdpHache = MotDePasse::hacher($motDePasse);

            $utilisateur = Utilisateur::create($login, $mdpHache, $email, $pictureName);
            $this->utilisateurRepositoryInterface->ajouter($utilisateur);
        }
    }
    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true) : ?Utilisateur {
        $utilisateur = $this->utilisateurRepositoryInterface->recupererParClePrimaire($idUtilisateur);
        if(!$autoriserNull && $utilisateur==null) {
            throw new ServiceException("L'utilisateur n'existe pas");
        }
        return $utilisateur;
    }
    public function connecterUtilisateur($login, $motDePasse): void {
        $utilisateur = $this->utilisateurRepositoryInterface->recupererParLogin($login);

        if ($utilisateur == null) {
            throw new ServiceException("Login inconnu");
        }
        if (!MotDePasse::verifier($motDePasse, $utilisateur->getMdpHache())) {
            throw new ServiceException("Mot de passe incorrect");
        }
        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
    }
    public function deconnecterUtilisateur(): void {
        if (!ConnexionUtilisateur::estConnecte()) {
            throw new ServiceException("Utilisateur non connecté");
        }
        ConnexionUtilisateur::deconnecter();
    }
}