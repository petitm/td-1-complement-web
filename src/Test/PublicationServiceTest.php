<?php
namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Service\PublicationService;
use TheFeed\Service\Exception\ServiceException;
use function PHPUnit\Framework\assertEquals;

class PublicationServiceTest extends TestCase {
    private $service;

    protected function setUp(): void
    {
        $this->service = new PublicationService();
    }
    public function testCreerPublicationUtilisateurInexistant(){
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage('non connecté');
        $message = "blabla";
        $this->service->creerPublication(-1, $message);
    }
    public function testCreerPublicationVide(){
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage('Message vide');
        $message = "";
        $this->service->creerPublication(14, $message);
    }
    public function testCreerPublicationTropGrande(){
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage('Message trop long');
        $message = str_repeat('a', 300);
        $this->service->creerPublication(14, $message);
    }
    public function testNombrePublications(){
        $publications = $this->service->recupererPublications();
        assertEquals(12,  count($publications));
    }
    public function testNombrePublicationsUtilisateur(){
        $publications = $this->service->recupererPublicationsUtilisateur(14);
        assertEquals(3,  count($publications));
    }
    public function testNombrePublicationsUtilisateurInexistant(){
        $publications = $this->service->recupererPublicationsUtilisateur(-1);
        assertEquals(0,  count($publications));
    }
}